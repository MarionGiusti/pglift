The files in this directory are the shell scripts that users can use to get
shell completion for pglift.

They have been generated using the commands described in the Click
shell-completion doc. (cf.
https://click.palletsprojects.com/en/8.0.x/shell-completion/#enabling-completion)
