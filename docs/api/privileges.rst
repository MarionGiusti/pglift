Default privileges
==================

.. currentmodule:: pglift.privileges

Module :mod:`pglift.privileges` exposes the following API to manipulate
default access privileges:

.. autofunction:: get
